<?php

declare(strict_types=1);

use SimpleSAML\Module;
use SimpleSAML\Module\perun\AttributeCheck;

/**
 * This is simple example of template where user has to accept usage policy
 *
 * Allow type hinting in IDE
 *
 * @var SimpleSAML\XHTML\Template $this
 */
$attributes = $this->data['attributes'];
$attributesGroupConfiguration = $this->data['attributes_group_config'];


$this->data['header'] = '';
$this->data['head'] = '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/res/bootstrap/css/bootstrap.css') . '" />';
$this->data['head'] .= '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/res/css/attribute_check.css') . '" />';
$this->data['head'] .= '<script src="' . Module::getModuleUrl('perun/res/js/jquery.js') . '" ></script>';
$this->data['head'] .= '<script src="'
    . Module::getModuleUrl('perun/res/bootstrap/js/bootstrap.js') . '" ></script>';

$this->includeAtTemplateBase('includes/header.php');

foreach ($attributesGroupConfiguration as $group) {
    echo AttributeCheck::handleAttributesGroup($this, $group, $attributes);
}

echo '<h4>' .  $this->t('{perun:attribute_check:attribute_details}') . '</h4>' . PHP_EOL;

echo "<div class='attributes_block' id='all_attributes'>";
foreach ($attributes as $attributeName => $attributeValue) {
    echo "<div class='row attribute_row'>" . PHP_EOL;
    echo "<div class='col-md-4 attribute_name'>" . PHP_EOL;
    echo '<div>' . $attributeName . '</div>' . PHP_EOL;
    echo '</div>' . PHP_EOL;

    echo "<div class='col-md-8 attribute_value'>" . PHP_EOL;
    if (count($attributeValue) > 1) {
        echo '<ul>' . PHP_EOL;
        foreach ($attributeValue as $value) {
            echo '<li>' . $value . '</li>' . PHP_EOL;
        }
        echo '</ul>' . PHP_EOL;
    } elseif (count($attributeValue) === 1) {
        echo '<div>' . $attributeValue[0] . '</div>' . PHP_EOL;
    } else {
        echo '<div>' . PHP_EOL . '</div>' . PHP_EOL;
    }

    echo '</div>' . PHP_EOL;
    echo '</div>' . PHP_EOL;
}
echo '</div>' . PHP_EOL;

$this->includeAtTemplateBase('includes/footer.php');
