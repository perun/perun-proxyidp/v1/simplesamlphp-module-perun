<?php

/**
 * This template just shows a time to metadata expiration.
 *
 * It can be used to check whether the meta refresh works without problems.
 */

declare(strict_types=1);

echo $this->data['closestExpiration'];
