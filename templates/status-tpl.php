<?php

/**
 * Template of page, which showing status of used components.
 */

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\StatusConnector;

$config = Configuration::getInstance();
$instanceName = $config->getString('instance_name', '');

$this->data['header'] = $instanceName . ' ' . $this->t('{perun:status:aai}') . ' ' . $this->t('{perun:status:header}');
$this->data['head'] = '<link rel="stylesheet"  media="screen" type="text/css" href="' .
                      Module::getModuleUrl('perun/res/css/status.css') . '" />';

$services = $this->data['services'];

$this->includeAtTemplateBase('includes/header.php');
?>


<p><?php echo $this->t('{perun:status:text}') . ' ' . $instanceName . ' ' . $this->t('{perun:status:aai}'); ?></p>
<div class="services">
    <?php foreach ($services as $service) { ?>
    <div class="row service">
        <div class="col col-md-9">
            <h3><?php echo $service['name'];?></h3>
            <?php if (isset($service['description']) && !empty($service['description'])) {?>
                <p><span class="glyphicon glyphicon-info-sign mr-1"></span><?php echo $service['description']; ?></p>
            <?php } ?>
        </div>
        <div class="col col-md-3 text-right">
            <h2 class="status-label">
                <?php echo StatusConnector::getBadgeByStatusString($service['status'], $this); ?>
            </h2>
        </div>
        <hr/>
    </div>
    <?php } ?>
</div>

<div class="row legend">
    <div class="col">
        <h4><?php echo $this->t('{perun:status:legend}'); ?></h4>
    </div>
    <div class="col-md-4">
        <?php echo StatusConnector::getLegendAndBadgeByStatus(StatusConnector::OK, $this); ?>
    </div>
    <div class="col-md-4">
        <?php echo StatusConnector::getLegendAndBadgeByStatus(StatusConnector::WARNING, $this); ?>
    </div>
    <div class="col-md-4">
        <?php echo StatusConnector::getLegendAndBadgeByStatus(StatusConnector::CRITICAL, $this); ?>
    </div>
</div>

<?php
$this->includeAtTemplateBase('includes/footer.php');
