<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\LshostelTemplateHelper;

$config = Configuration::getInstance();

if (!isset($_POST['usernameReminderOk'])) {
    $_POST['usernameReminderOk'] = false;
}

$this->data['head'] .=
    '<script src="' . Module::getModuleUrl('perun/res/js/jquery.js') . '" ></script>';
$this->data['head'] .=
    '<script src="' . Module::getModuleUrl('perun/res/bootstrap/js/bootstrap.min.js') . '" ></script>';
$this->data['head'] .= '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/lshostel/res/css/lshostel.css') . '" />';

$this->data['header'] = $this->t('{perun:lshostel:username_reminder_header}');

$this->includeAtTemplateBase('includes/header.php');

?>
<div class="row pl-0 pr-0">
    <div class="col-xs-12">
        <?php
        $userEmail = '';
        if (isset($_POST['email'])) {
            $userEmail = $_POST['email'];
            try {
                if (!$_POST['usernameReminderOk']) {
                    LshostelTemplateHelper::sendUsernameReminder($userEmail);
                    $_POST['usernameReminderOk'] = true;
                    unset($_POST['email']);
                } ?>
                <div class="alert alert-success">
                    <p>
                        <span class="glyphicon glyphicon-exclamation-sign"
                              style="float:left; font-size: 38px; margin-right: 10px;"></span>
                        <strong><?php
                            echo $this->t('{perun:lshostel:username_reminder_ok_header}'); ?></strong>
                    </p>
                    <p><?php
                        echo $this->t('{perun:lshostel:username_reminder_ok_text}'); ?></p>
                </div>

                <?php
            } catch (\Exception $exception) {
                Logger::error('username_reminder-tpl.php - ' . $exception->getMessage());
                $emailAddress = $config->getString('technicalcontact_email');
                if (!str_starts_with('mailto:', $emailAddress)) {
                    $emailAddress = 'mailto:' . $emailAddress;
                } ?>
                <div class="alert alert-danger">
                    <span class="glyphicon glyphicon-exclamation-sign"
                          style="float:left; font-size: 38px; margin-right: 10px;"></span>
                    <strong><?php
                        echo $this->t('{perun:lshostel:username_reminder_err_header}'); ?></strong>
                    <p><?php
                        echo $this->t('{perun:lshostel:username_reminder_err_text_part1}'); ?>
                        <a href="<?php
                          echo $emailAddress; ?>">
                          <?php echo $this->t('{perun:lshostel:username_reminder_support}'); ?></a>.
                          <?php echo $this->t('{perun:lshostel:username_reminder_err_text_part2}'); ?>
                    </p>
                </div>

                <?php
            }
        }

        if (!$_POST['usernameReminderOk']) {
            ?>

            <p class="text-center"><?php echo $this->t('{perun:lshostel:username_reminder_text}'); ?></p>
            <form action="" method="post" name="username_reminder" class="form-horizontal">
                <div class="form-group">
                    <label class="sr-only" for="inlineFormInputGroup"><?php
                        echo $this->t('{perun:lshostel:username_reminder_email}'); ?></label>
                    <div class="input-group mb-2">
                    <span class="input-group-addon">
                            <span class=" glyphicon glyphicon-envelope" id="basic-addon1"></span>
                    </span>
                        <input id="email" name="email" class="form-control" value="<?php
                        echo $userEmail; ?>" placeholder="Email address" aria-describedby="basic-addon1"/>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-success btn-block" type="submit">
                        <?php
                        echo $this->t('{perun:lshostel:username_reminder_submit}'); ?>
                    </button>
                </div>
              <div class="form-group text-center">
                <a class="btn btn-link" href="<?php
                echo SimpleSAML\Module::getModuleURL("perun/lshostel/pwd_reset.php");?>">
                    <?php
                    echo $this->t('{perun:lshostel:username_reminder_button_forgotten_password}')?>
                </a>
              </div>
            </form>

            <?php
        }
        ?>
    </div>

<?php

$this->includeAtTemplateBase('includes/footer.php');
