<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\LshostelTemplateHelper;

$config = Configuration::getInstance();

if (!isset($_POST['passwordResetOk'])) {
    $_POST['passwordResetOk'] = false;
}


$this->data['head'] .=
    '<script src="' . Module::getModuleUrl('perun/res/js/jquery.js') . '" ></script>';
$this->data['head'] .=
    '<script src="' . Module::getModuleUrl('perun/res/bootstrap/js/bootstrap.min.js') . '" ></script>';
$this->data['head'] .= '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/lshostel/res/css/lshostel.css') . '" />';

$this->data['header'] = $this->t('{perun:lshostel:pwd_reset_header}');

$this->includeAtTemplateBase('includes/header.php');

?>
<div class="row pl-0 pr-0">
    <div class="col-xs-12">
    <?php
    $userName = '';
    if (isset($_POST['username'])) {
        $userName = $_POST['username'];
        try {
            if (!$_POST['passwordResetOk']) {
                LshostelTemplateHelper::sendPasswordResetEmail($userName);
                $_POST['passwordResetOk'] = true;
                unset($_POST['username']);
            } ?>
            <div class="alert alert-success">
                <p>
                    <span class="glyphicon glyphicon-exclamation-sign"
                          style="float:left; font-size: 38px; margin-right: 10px;"></span>
                    <strong><?php echo $this->t('{perun:lshostel:pwd_reset_ok_header}'); ?></strong>
                </p>
                <p><?php echo $this->t('{perun:lshostel:pwd_reset_ok_text}'); ?></p>
            </div>

            <?php
        } catch (\Exception $exception) {
            $emailAddress = $config->getString('technicalcontact_email');
            if (!str_starts_with('mailto:', $emailAddress)) {
                $emailAddress = 'mailto:' . $emailAddress;
            } ?>
            <div class="alert alert-danger">
                <span class="glyphicon glyphicon-exclamation-sign"
                      style="float:left; font-size: 38px; margin-right: 10px;"></span>
                <strong><?php
                    echo $this->t('{perun:lshostel:pwd_reset_err_header}'); ?></strong>
                <p><?php
                    echo $this->t('{perun:lshostel:pwd_reset_err_text_part1}'); ?></p>
                <p><?php
                    echo $this->t('{perun:lshostel:pwd_reset_err_text_part2}'); ?>
                    <a href="<?php
                    echo $emailAddress; ?>"><?php
                        echo $this->t('{perun:lshostel:pwd_reset_support}'); ?></a>.
                </p>
            </div>

            <?php
        }
    }

    if (!$_POST['passwordResetOk']) {
        ?>

        <p class="text-center"><?php echo $this->t('{perun:lshostel:pwd_reset_text}'); ?></p>
        <form action="" method="post" name="passwd_reset" class="form-horizontal">
            <div class="form-group">
                <label class="sr-only" for="inlineFormInputGroup"><?php
                    echo $this->t('{{perun:lshostel:pwd_reset_username}'); ?></label>
                <div class="input-group mb-2">
                <span class="input-group-addon">
                        <span class=" glyphicon glyphicon-user" id="basic-addon1"></span>
                </span>
                    <input id="username" name="username" class="form-control" value="<?php
                    echo $userName; ?>" placeholder="Username" aria-describedby="basic-addon1"/>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success btn-block" type="submit">
                    <?php
                    echo $this->t('{perun:lshostel:pwd_reset_submit}'); ?>
                </button>
            </div>
          <div class="form-group text-center">
            <a class="btn btn-link" href="<?php
            echo SimpleSAML\Module::getModuleURL("perun/lshostel/username_reminder.php");?>">
                <?php echo $this->t('{perun:lshostel:pwd_reset_button_forgotten_username}')?>
            </a>
          </div>
        </form>

    <?php } ?>
    </div>
</div>

<?php

$this->includeAtTemplateBase('includes/footer.php');
