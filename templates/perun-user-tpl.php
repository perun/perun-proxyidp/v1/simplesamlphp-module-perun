<?php

declare(strict_types=1);

use SimpleSAML\Module\perun\Auth\Process\PerunUser;

$this->data['header'] = $this->t('{perun:perun:register_header}');

$this->includeAtTemplateBase('includes/header.php');

?>

<div class="row">
    <div>
        <p><?php echo $this->t('{perun:perun:register_text}'); ?></p>
        <form action="<?php echo htmlspecialchars($this->data[PerunUser::PARAM_REGISTRATION_URL]); ?>" method="GET">
            <?php foreach ($this->data[PerunUser::PARAM_PARAMS] as $key => $value) { ?>
                <input type="hidden" name="<?php echo htmlspecialchars($key); ?>"
                       value="<?php echo htmlspecialchars($value); ?>">
            <?php } ?>
            <input type="submit" class="btn btn-lg btn-block btn-primary"
                   value="<?php echo $this->t('{perun:perun:register_button}'); ?>">
        </form>
    </div>
</div>


<?php

$this->includeAtTemplateBase('includes/footer.php');
