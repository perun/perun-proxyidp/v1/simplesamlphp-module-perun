<?php

namespace SimpleSAML\Module\perun;

use Exception;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Module\perun\Exception as PerunException;

class UESUpdateHelper
{
    public const DEBUG_PREFIX = 'perun/www/updateUes.php: ';
    public const UID = 'uid';
    public const CONFIG_FILE_NAME = 'module_perun.php';
    public const CONFIG_SECTION = 'updateUes';
    public const SOURCE_IDP_ATTRIBUTE_KEY = 'sourceIdPAttributeKey';
    public const USER_IDENTIFIERS = 'userIdentifiers';
    public const SOURCE_IDP_ENTITY_ID = 'sourceIdPEntityID';
    public const NAME = 'name';
    public const FRIENDLY_NAME = 'friendlyName';
    public const ID = 'id';
    public const VALUE = 'value';
    public const NAMESPACE_KEY = 'namespace';
    public const UES_ATTR_NMS = 'urn:perun:ues:attribute-def:def';
    public const TYPE = 'type';
    public const STRING_TYPE = 'String';
    public const INTEGER_TYPE = 'Integer';
    public const BOOLEAN_TYPE = 'Boolean';
    public const ARRAY_TYPE = 'Array';
    public const MAP_TYPE = 'Map';
    public const DATA = 'data';
    public const ATTRIBUTES = 'attributes';
    public const ATTR_MAP = 'attrMap';
    public const ATTR_TO_CONVERSION = 'attrsToConversion';
    public const APPEND_ONLY_ATTRS = 'appendOnlyAttrs';
    public const PERUN_USER_ID = 'perunUserId';
    public const EDU_PERSON_UNIQUE_ID = 'eduPersonUniqueId';
    public const EDU_PERSON_PRINCIPAL_NAME = 'eduPersonPrincipalName';
    public const EDU_PERSON_TARGETED_ID = 'eduPersonTargetedID';
    public const NAMEID = 'nameid';

    public static function getDefaultConfig(): array
    {
        return [
            self::SOURCE_IDP_ATTRIBUTE_KEY => self::SOURCE_IDP_ENTITY_ID,
            self::USER_IDENTIFIERS => [
                self::EDU_PERSON_UNIQUE_ID,
                self::EDU_PERSON_PRINCIPAL_NAME,
                self::EDU_PERSON_TARGETED_ID,
                self::NAMEID,
                self::UID
            ],
        ];
    }

    public static function getConfiguration()
    {
        $config = self::getDefaultConfig();
        try {
            $configuration = Configuration::getConfig(self::CONFIG_FILE_NAME);
            $localConfig = $configuration->getArray(self::CONFIG_SECTION, null);
            if (!empty($localConfig)) {
                $config = $localConfig;
            } else {
                Logger::warning(self::DEBUG_PREFIX . 'Configuration is missing. Using default values');
            }
        } catch (Exception $e) {
            Logger::warning(self::DEBUG_PREFIX . 'Configuration is invalid. Using default values');
            //OK, we will use the default config
        }

        return $config;
    }

    public static function findUserExtSource($adapter, $extSourceName, $attributesFromIdp, $identifierAttributes)
    {
        foreach ($attributesFromIdp as $attrName => $attrValue) {
            if (!in_array($attrName, $identifierAttributes, true)) {
                continue;
            }

            if (!is_array($attrValue)) {
                $attrValue = [$attrValue];
            }

            if (empty($attrValue)) {
                continue;
            }

            foreach ($attrValue as $extLogin) {
                if (empty($extLogin)) {
                    continue;
                }
                $userExtSource = self::getUserExtSource($adapter, $extSourceName, $extLogin);
                if ($userExtSource !== null) {
                    Logger::debug(
                        self::DEBUG_PREFIX . 'Found user ext source for combination extSourceName \''
                        . $extSourceName . '\' and extLogin \'' . $extLogin . '\''
                    );
                    return $userExtSource;
                }
            }
        }

        return null;
    }

    public static function getUserExtSource($adapter, $extSourceName, $extLogin)
    {
        try {
            return $adapter->getUserExtSource($extSourceName, $extLogin);
        } catch (PerunException $ex) {
            Logger::debug(
                self::DEBUG_PREFIX . 'Did not find extSource for combination of parameters extSourceName: \''
                . $extSourceName . '\', extLogin: \'' . $extLogin . '\''
            );
            return null;
        }
    }

    public static function getAttributesFromPerun($adapter, $attrMap, $userExtSource): array
    {
        $attributesFromPerun = [];
        $attributesFromPerunRaw =
            $adapter->getUserExtSourceAttributes($userExtSource[self::ID], array_keys($attrMap));
        if (empty($attributesFromPerunRaw)) {
            throw new Exception(self::DEBUG_PREFIX . 'Getting attributes for UES was not successful.');
        }

        foreach ($attributesFromPerunRaw as $rawAttribute) {
            if (!empty($rawAttribute[self::NAME])) {
                $attributesFromPerun[$rawAttribute[self::NAME]] = $rawAttribute;
            }
        }

        if (empty($attributesFromPerun)) {
            throw new Exception(
                sprintf("%sGetting attributes for UES was not successful.", self::DEBUG_PREFIX)
            );
        }

        return $attributesFromPerun;
    }

    public static function getAttributesToUpdate(
        $attributesFromPerun,
        $attrMap,
        $serializedAttributes,
        $appendOnlyAttrs,
        $attributesFromIdP
    ): array {
        $attributesToUpdate = [];

        foreach ($attributesFromPerun as $attribute) {
            $attrName = $attribute[self::NAME];

            $attr = !empty($attributesFromIdP[$attrMap[$attrName]]) ?
                $attributesFromIdP[$attrMap[$attrName]] : [];

            // appendOnly && has value && (complex || serialized)
            if (
                in_array($attrName, $appendOnlyAttrs, true) &&
                !empty($attribute[self::VALUE]) &&
                (self::isComplexType($attribute[self::TYPE]) ||
                  in_array($attrName, $serializedAttributes, true))
            ) {
                if (
                    strpos($attribute[self::TYPE], self::MAP_TYPE) ||
                    !in_array($attrName, $serializedAttributes, true)
                ) {
                    $attr = array_merge($attribute[self::VALUE], $attr);
                } elseif (self::isComplexType($attribute[self::TYPE])) {
                    $attr = array_merge($attr, $attribute[self::VALUE]);
                } else {
                    $attr = array_merge($attr, explode(';', $attribute[self::VALUE]));
                }
            }

            if (self::isSimpleType($attribute[self::TYPE])) {
                $newValue = self::convertToString($attr);
            } elseif (self::isComplexType($attribute[self::TYPE])) {
                if (empty($attr)) {
                    $newValue = [];
                } elseif (strpos($attribute[self::TYPE], self::MAP_TYPE)) {
                    $newValue = $attr;
                } else {
                    $attr = array_filter($attr);
                    $newValue = array_values(array_unique($attr));
                }
                if (in_array($attrName, $serializedAttributes, true)) {
                    $newValue = self::convertToString($newValue);
                }
            } else {
                Logger::debug(self::DEBUG_PREFIX . 'Unsupported type of attribute.');
                continue;
            }

            if ($newValue !== $attribute[self::VALUE]) {
                $attribute[self::VALUE] = $newValue;
                $attribute[self::NAMESPACE_KEY] = self::UES_ATTR_NMS;
                $attributesToUpdate[] = $attribute;
            }
        }

        return $attributesToUpdate;
    }

    public static function updateUserExtSource($adapter, $userExtSource, $attributesToUpdate): bool
    {
        $attributesToUpdateFinal = [];

        if (!empty($attributesToUpdate)) {
            foreach ($attributesToUpdate as $attribute) {
                $attribute[self::NAME] =
                    self::UES_ATTR_NMS . ':' . $attribute[self::FRIENDLY_NAME];
                $attributesToUpdateFinal[] = $attribute;
            }

            $adapter->setUserExtSourceAttributes($userExtSource[self::ID], $attributesToUpdateFinal);
        }

        $adapter->updateUserExtSourceLastAccess($userExtSource[self::ID]);

        return true;
    }

    public static function isSimpleType($attributeType): bool
    {
        return strpos($attributeType, self::STRING_TYPE)
            || strpos($attributeType, self::INTEGER_TYPE)
            || strpos($attributeType, self::BOOLEAN_TYPE);
    }

    public static function isComplexType($attributeType): bool
    {
        return strpos($attributeType, self::ARRAY_TYPE) ||
            strpos($attributeType, self::MAP_TYPE);
    }

    public static function convertToString($newValue): string
    {
        if (!empty($newValue)) {
            $newValue = array_unique($newValue);
            $attrValueAsString = implode(';', $newValue);
        } else {
            $attrValueAsString = '';
        }

        return $attrValueAsString;
    }
}
