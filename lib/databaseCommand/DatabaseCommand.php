<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\databaseCommand;

use Exception;
use SimpleSAML\Database;
use SimpleSAML\Logger;
use SimpleSAML\Module\perun\databaseCommand\DatabaseConfig;

abstract class DatabaseCommand
{
    private const STAGE = 'perun:DatabaseCommand';

    private const DEBUG_PREFIX = self::STAGE . ' - ';

    protected $config;

    private $conn;

    /** @var string Character used to quote SQL identifiers. Default to " per SQL:1999 */
    private $sqlIdentifierQuoteChar = '"';

    public function __construct()
    {
        $this->config = DatabaseConfig::getInstance();
        $this->conn = Database::getInstance($this->config->getStore());
        if ($this->isMysql()) {
            $this->sqlIdentifierQuoteChar = '`';
        } elseif (!$this->isPgsql()) {
            $this->unknownDriver();
        }
    }

    protected function read($query, $params)
    {
        return $this->conn->read($query, $params);
    }

    protected function write($query, $params): bool
    {
        $response = $this->conn->write($query, $params);
        if (is_int($response)) {
            Logger::debug(self::DEBUG_PREFIX . "Inserted into DB");
            return true;
        }
        Logger::debug(self::DEBUG_PREFIX . "Not inserted into DB");
        return false;
    }

    public function isMysql(): bool
    {
        return $this->conn->getDriver() === 'mysql';
    }

    public function isPgsql(): bool
    {
        return $this->conn->getDriver() === 'pgsql';
    }

    public function q()
    {
        return $this->sqlIdentifierQuoteChar;
    }

    protected function unknownDriver()
    {
        Logger::error(self::DEBUG_PREFIX . 'unsupported DB driver \'' . $this->conn->getDriver());
        throw new Exception('Unsupported DB driver');
    }
}
