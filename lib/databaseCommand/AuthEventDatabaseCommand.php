<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\databaseCommand;

class AuthEventDatabaseCommand extends DatabaseCommand
{
    private const STAGE = 'perun:AuthEventDatabaseCommand';

    private const DEBUG_PREFIX = self::STAGE . ' - ';

    public function insertIntoAuthTable($table, $params)
    {

        // Remove NULL values from params along with keys
        $params = array_filter($params, function ($v) {
            return $v !== null;
        });

        // Main query with foreign keys
        $qKeys = "";
        $qValKeys = "";
        $reservedNames = ["day", "user"];

        foreach (array_keys($params) as $k) {
            $newKey = in_array($k, $reservedNames) ? ($this->q() . $k . $this->q()) : $k;
            $qKeys .= ($qKeys != "" ? ", " : "") . $newKey;
            $qValKeys .= ($qValKeys != "" ? ", " : "") . ":" . $k;
        }
        $query = 'INSERT INTO ' . $table . ' ';
        $query .= '(' . $qKeys . ') VALUES (' . $qValKeys . ')';

        parent::write($query, $params);
    }

    public function getIdFromIdentifier($table, $identifier, $name)
    {

        // Check if valid value to search
        if (empty($identifier)) {
            return null;
        }

        $query = "INSERT INTO " . $table . " (identifier, name) VALUES (:identifier, :name1) ";
        if (parent::isPgsql()) {
            $query .= 'ON CONFLICT (identifier) DO UPDATE SET name = :name2;';
        } elseif (parent::isMysql()) {
            $query .= 'ON DUPLICATE KEY UPDATE name = :name2';
        } else {
            parent::unknownDriver();
        }

        parent::write($query, [
            'identifier' => $identifier,
            'name1' => $name,
            'name2' => $name,
        ]);

        // Get ID after inserting
        return parent::read('SELECT id FROM ' . $table
            . ' WHERE identifier=:identifier', [
                'identifier' => $identifier,
            ])->fetchColumn();
    }

    public function getIdFromForeignTable($table, $value)
    {

        // Check if valid value to search
        if (empty($value)) {
            return null;
        }

        $insertQuery = 'INSERT INTO ' . $table . ' (value) VALUES (:value1) ';

        if (parent::isPgsql()) {
            $insertQuery .= 'ON CONFLICT DO NOTHING';
            parent::write($insertQuery, [
                'value1' => $value
            ]);
        } elseif (parent::isMysql()) {
            // Does not trigger row update with same values
            $insertQuery .= 'ON DUPLICATE KEY UPDATE value (:value2)';
            parent::write($insertQuery, [
                'value1' => $value,
                'value2' => $value
            ]);
        } else {
            parent::unknownDriver();
        }

        // Get ID after inserting
        $readQuery = 'SELECT id FROM ' . $table . ' WHERE value=:value';
        return parent::read($readQuery, [
            'value' => $value
        ])->fetchColumn();
    }
}
