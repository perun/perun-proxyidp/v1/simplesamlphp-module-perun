<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\databaseCommand;

use SimpleSAML\Configuration;

class DatabaseConfig
{
    private const CONFIG_FILE_NAME = 'module_perun.php';

    private const DATABASE = 'database';

    private const STORE = 'store';

    private $config;

    private $store;

    private static $instance;

    private function __construct()
    {
        $configuration = Configuration::getConfig(self::CONFIG_FILE_NAME);

        $this->config = $configuration->getConfigItem(self::DATABASE);
        $this->store = $this->config->getConfigItem(self::STORE, null);
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getStore()
    {
        return $this->store;
    }
}
