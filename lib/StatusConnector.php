<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun;

use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\XHTML\Template;

/**
 * Abstract class StatusConnector specify interface to get status information about some components.
 */
abstract class StatusConnector
{
    public const NAGIOS = 'NAGIOS';

    public const CONFIG_FILE_NAME = 'module_perun.php';

    public const STATUS_TYPE = 'status.type';

    public const OK = 0;

    public const WARNING = 1;
    public const CRITICAL = 2;

    protected $configuration;

    public function __construct()
    {
        $this->configuration = Configuration::getConfig(self::CONFIG_FILE_NAME);
    }

    /**
     * @return StatusConnector instance
     */
    public static function getInstance()
    {
        $configuration = Configuration::getConfig(self::CONFIG_FILE_NAME);
        $statusType = $configuration->getString(self::STATUS_TYPE, 'NAGIOS');
        if ($statusType === self::NAGIOS) {
            return new NagiosStatusConnector();
        }
        throw new Exception(
            'Unknown StatusConnector type in option \'' . self::STATUS_TYPE
            . '\'. Only ' . self::NAGIOS . ' type available now!'
        );
    }

    /**
     * Returns list of components with statuses in this format: [ [ 'name' => 'Component name', 'status' => 'Component
     * status' ], ],.
     *
     * @return array
     */
    abstract public function getStatus();

    /**
     * Resolve HTML for label with correct class based on passed status
     * @param string $status status
     * @param Template $t template with localization
     * @return string HTML to be displayed
     */
    public static function getBadgeByStatusString(string $status, Template $t)
    {
        $statusAsInt = intval($status);
        return self::getBadgeByStatus($statusAsInt, $t);
    }

    /**
     * Resolve HTML for label with correct class based on passed status
     * @param int $status status
     * @param Template $t template with localization
     * @return string HTML to be displayed
     */
    public static function getBadgeByStatus(int $status, Template $t): string
    {
        if ($status === self::OK) {
            return '<span class="status label label-success">' . $t->t('{perun:status:status_ok}') . '</span>';
        }
        if ($status === self::WARNING) {
            return '<span class="status label label-warning">' . $t->t('{perun:status:status_warning}') . '</span>';
        } else {
            return '<span class="status label label-danger">' . $t->t('{perun:status:status_critical}') . '</span>';
        }
    }

    /**
     * Resolve HTML for legend with correct classes based on passed status
     * @param int $status status
     * @param Template $t template with localization
     * @return string HTML to be displayed
     */
    public static function getLegendAndBadgeByStatus(int $status, Template $t): string
    {
        $html = '<div class="row">' . PHP_EOL;
        $html .=
            '<h4 class="col status-label text-center">'
            . StatusConnector::getBadgeByStatus($status, $t)
            . '</h4>'
            . PHP_EOL;
        $html .= '<div class="col text-center">';
        if ($status === self::OK) {
            $html .= $t->t('{perun:status:status_ok_legend}');
        } elseif ($status === self::WARNING) {
            $html .= $t->t('{perun:status:status_warning_legend}');
        } else {
            $html .= $t->t('{perun:status:status_critical_legend}');
        }
        $html .= '</div>' . PHP_EOL;
        $html .= '</div>' . PHP_EOL;

        return $html;
    }
}
