<?php

namespace SimpleSAML\Module\perun;

class CommonUtils
{
    public static function substrInArray($needle, array $haystack)
    {
        foreach ($haystack as $item) {
            if (strpos($item, $needle) !== false) {
                return $item;
            }
        }

        return null;
    }

    public static function getFacilityFromRequest($request, $adapter)
    {
        $spEntityId = $request['SPMetadata']['entityid'];
        $clientIdWithPrefix = !isset($request[PerunConstants::REQUESTER_ID]) ? null :
            CommonUtils::substrInArray(PerunConstants::CLIENT_ID_PREFIX, $request[PerunConstants::REQUESTER_ID]);

        if (!is_null($clientIdWithPrefix)) {
            $clientId = str_replace(PerunConstants::CLIENT_ID_PREFIX, '', $clientIdWithPrefix);
            $facility = $adapter->getFacilityByClientId($clientId);
        } else {
            $facility = $adapter->getFacilityByEntityId($spEntityId);
        }
        return $facility;
    }
}
