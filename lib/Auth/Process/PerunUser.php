<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Adapter;
use SimpleSAML\Module\perun\model\User;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\Utils\HTTP;

/**
 * Class tries to find user in Perun using the extLogin and extSourceName (in case of RPC adapter).
 *
 * If the user cannot be found, it redirects user to the registration URL.
 */
class PerunUser extends ProcessingFilter
{
    public const STAGE = 'perun:PerunUser';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const CALLBACK = 'perun/perun_user_callback.php';

    public const REDIRECT = 'perun/perun_user.php';

    public const TEMPLATE = 'perun:perun-user-tpl.php';

    public const PARAM_REGISTRATION_URL = 'registrationUrl';
    public const PARAM_REGISTRATION_VO = 'registrationVo';
    public const PARAM_REGISTRATION_GROUP = 'registrationGroup';
    public const PARAM_PARAMS = 'params';

    public const PARAM_STATE_ID = PerunConstants::STATE_ID;

    public const INTERFACE = 'interface';

    public const UID_ATTRS = 'uid_attrs';

    public const IDP_ID_ATTR = 'idp_id_attr';

    public const REGISTER_URL = 'register_url';

    public const CALLBACK_PARAMETER_NAME = 'callback_parameter_name';

    public const PERUN_REGISTER_URL = 'perun_register_url';
    public const PERUN_REGISTER_VO = 'perun_register_vo';
    public const PERUN_REGISTER_GROUP = 'perun_register_group';

    public const USE_ADDITIONAL_IDENTIFIERS_LOOKUP = 'use_additional_identifiers_lookup';

    public const ADDITIONAL_IDENTIFIERS_ATTRIBUTE = 'additional_identifiers_attribute';

    public const USE_REGULAR_IDS_IN_ADDITIONAL_USER_IDS = 'use_regular_ids_in_auids';

    private $adapter;

    private $idpEntityIdAttr;

    private $userIdAttrs;

    private $registerUrl;

    private $callbackParameterName;

    private $perunRegisterUrl;
    private $perunRegisterVo;
    private $perunRegisterGroup;

    private $useAdditionalIdentifiersLookup;

    private $additionalIdentifiersAttribute;

    private $useRegularIdsInAuids;

    private $config;

    private $filterConfig;

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $this->config = $config;
        $this->filterConfig = Configuration::loadFromArray($config);

        $interface = $this->filterConfig->getString(self::INTERFACE, Adapter::RPC);

        $this->adapter = Adapter::getInstance($interface);
        $this->userIdAttrs = $this->filterConfig->getArray(self::UID_ATTRS, []);
        if (empty($this->userIdAttrs)) {
            throw new Exception(
                self::DEBUG_PREFIX .
                'Invalid configuration: no attributes configured for extracting UID. Use option \'' . self::UID_ATTRS .
                '\' to configure list of attributes, that should be considered as IDs for a user'
            );
        }
        $this->idpEntityIdAttr = $this->filterConfig->getString(self::IDP_ID_ATTR, null);
        if (empty($this->idpEntityIdAttr)) {
            throw new Exception(
                sprintf(
                    "%sInvalid configuration: no attribute containing IDP ID has been configured."
                    . " Use option '%s' to configure the name of the attribute, that has been previously used"
                    . "in the configuration of filter 'perun:ExtractIdpEntityId'",
                    self::DEBUG_PREFIX,
                    self::IDP_ID_ATTR
                )
            );
        }

        $this->perunRegisterUrl = $this->filterConfig->getString(self::PERUN_REGISTER_URL, null);
        $this->perunRegisterVo = $this->filterConfig->getString(self::PERUN_REGISTER_VO, null);
        $this->perunRegisterGroup = $this->filterConfig->getString(self::PERUN_REGISTER_GROUP, null);
        if (!empty($this->perunRegisterUrl) && empty($this->perunRegisterVo)) {
            throw new Exception(
                sprintf(
                    "%sInvalid configuration: when using the Perun registrar, use the option '%s' to configure the
                    registrar URL, %s to configure the VO (mandatory), %s to configure the group (optional).",
                    self::DEBUG_PREFIX,
                    self::PERUN_REGISTER_URL,
                    self::PERUN_REGISTER_VO,
                    self::PERUN_REGISTER_GROUP
                )
            );
        }

        $this->registerUrl = $this->filterConfig->getString(self::REGISTER_URL, null);
        $this->callbackParameterName = $this->filterConfig->getString(self::CALLBACK_PARAMETER_NAME, null);
        if (empty($this->registerUrl) && empty($this->callbackParameterName) && empty($this->perunRegisterUrl)) {
            throw new Exception(
                sprintf(
                    "%sInvalid configuration: no URL where user should register for an account has been configured. 
                    Use either  option '%s' or '%s'",
                    self::DEBUG_PREFIX,
                    self::REGISTER_URL,
                    self::PERUN_REGISTER_URL
                )
            );
        }
        $this->useAdditionalIdentifiersLookup = $this->filterConfig->getBoolean(
            self::USE_ADDITIONAL_IDENTIFIERS_LOOKUP,
            false
        );
        $this->additionalIdentifiersAttribute = $this->filterConfig->getString(
            self::ADDITIONAL_IDENTIFIERS_ATTRIBUTE,
            null
        );
        $this->useRegularIdsInAuids = $this->filterConfig->getBoolean(
            self::USE_REGULAR_IDS_IN_ADDITIONAL_USER_IDS,
            false
        );
        if (
            $this->useAdditionalIdentifiersLookup
            && $this->additionalIdentifiersAttribute === null
            && !$this->useRegularIdsInAuids
        ) {
            throw new Exception(
                sprintf(
                    "%sInvalid configuration: no attribute configured for extracting additional identifiers,"
                    . " nor regular IDs have been enabled to be used. Use option '%s' to configure the name of the "
                    . " attribute, that should be considered as additional identifiers of the user use option '%s'"
                    . " to enable usage of regular IDs (e.g. EPPN).",
                    self::DEBUG_PREFIX,
                    self::ADDITIONAL_IDENTIFIERS_ATTRIBUTE,
                    self::USE_REGULAR_IDS_IN_ADDITIONAL_USER_IDS
                )
            );
        }
    }

    public function process(&$request)
    {
        assert(is_array($request));
        assert(array_key_exists(PerunConstants::ATTRIBUTES, $request));

        $uids = [];
        foreach ($this->userIdAttrs as $uidAttr) {
            if (isset($request[PerunConstants::ATTRIBUTES][$uidAttr][0])) {
                $uids[] = $request[PerunConstants::ATTRIBUTES][$uidAttr][0];
            }
        }
        if (empty($uids)) {
            $serializedUids = implode(', ', $this->userIdAttrs);
            throw new Exception(
                self::DEBUG_PREFIX . 'missing at least one of mandatory attributes [' . $serializedUids .
                '] in request.'
            );
        }

        if (!empty($request[PerunConstants::ATTRIBUTES][$this->idpEntityIdAttr][0])) {
            $idpEntityId = $request[PerunConstants::ATTRIBUTES][$this->idpEntityIdAttr][0];
        } else {
            throw new Exception(
                self::DEBUG_PREFIX . 'Cannot find entityID of source IDP. Did you properly configure ' .
                ExtractRequestAttribute::STAGE . ' filter before this filter in the processing chain?'
            );
        }

        $user = $this->adapter->getPerunUser($idpEntityId, $uids);
        if ($this->useAdditionalIdentifiersLookup && $user === null) {
            $additionalIdentifiers =
                $request[PerunConstants::ATTRIBUTES][$this->additionalIdentifiersAttribute] ?? [];
            if ($this->useRegularIdsInAuids) {
                $additionalIdentifiers = array_merge($additionalIdentifiers, $uids);
            }
            if (empty($additionalIdentifiers)) {
                throw new Exception(
                    self::DEBUG_PREFIX . 'missing mandatory attribute [' . $this->additionalIdentifiersAttribute .
                    '] in request.'
                );
            }
            $user = $this->adapter->getPerunUserByAdditionalIdentifiers($idpEntityId, $additionalIdentifiers);
        }

        if (!empty($user)) {
            $this->processUser($request, $user, $uids);
        } else {
            $this->register($request);
        }
    }

    private function processUser(array &$request, User $user, array $uids): void
    {
        if (!isset($request[PerunConstants::PERUN])) {
            $request[PerunConstants::PERUN] = [];
        }

        $request[PerunConstants::PERUN][PerunConstants::USER] = $user;

        $logUids = implode(', ', $uids);
        Logger::info(
            self::DEBUG_PREFIX . 'Perun user with identity/ies: \'' . $logUids . '\' has been found. Setting user ' .
            $user->getName() . ' with id: ' . $user->getId() . ' to the request.'
        );
    }

    private function register(array &$request): void
    {
        $request[PerunConstants::CONTINUE_FILTER_CONFIG] = $this->config;
        if (!empty($this->registerUrl)) {
            $redirectUrl = $this->registerUrl;
            $params = [];
            if (!empty($this->callbackParameterName)) {
                $stateId = State::saveState($request, self::STAGE);
                $callback = Module::getModuleURL(self::CALLBACK, [
                    self::PARAM_STATE_ID => $stateId,
                ]);
                Logger::debug(
                    self::DEBUG_PREFIX . 'Redirecting to \'' . $this->registerUrl . ', callback parameter \''
                    . $this->callbackParameterName . '\' with value \'' . $callback . '\''
                );
                $params[$this->callbackParameterName] = $callback;
            }
            HTTP::redirectTrustedURL($redirectUrl, $params);
        } elseif (!empty($this->perunRegisterUrl)) {
            $request[self::STAGE] = [
                self::PARAM_REGISTRATION_URL => $this->perunRegisterUrl,
                self::PARAM_REGISTRATION_VO => $this->perunRegisterVo,
                self::PARAM_REGISTRATION_GROUP => $this->perunRegisterGroup
            ];
            $stateId = State::saveState($request, self::STAGE);

            $redirectUrl = Module::getModuleURL(self::REDIRECT, [self::PARAM_STATE_ID => $stateId]);
            Logger::debug(self::DEBUG_PREFIX . 'Redirecting to \'' . $redirectUrl . '\'');
            HTTP::redirectTrustedURL($redirectUrl);
        } else {
            throw new Exception(self::DEBUG_PREFIX . 'No configuration for User registration enabled. Cannot proceed');
        }
    }
}
