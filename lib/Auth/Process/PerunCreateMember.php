<?php

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\AdapterRpc;
use SimpleSAML\Module\perun\Exception as PerunException;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\Utils\HTTP;

class PerunCreateMember extends ProcessingFilter
{
    public const TEMPLATE_URL = 'perun:perun-create-member-tpl.php';

    public const REDIRECT_URL = 'perun/perun_create_member.php';

    public const CALLBACK_URL = 'perun/perun_create_member_callback.php';

    public const STAGE = 'perun:PerunCreateMember';

    public const PARAM_WAS_EXCEPTION = 'was_exception';

    public const PARAM_STATE_ID = PerunConstants::STATE_ID;

    public const PARAM_CALLBACK = 'callback';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const UID_ATTR = 'uid_attr';

    public const IDP_ENTITY_ID = 'idp_entity_id';

    public const VO_SHORT_NAME = 'vo_short_name';

    private $config;

    private $userIdAttr;

    private $adapter;

    private $idpEntityId;

    private $voShortName;

    public function __construct($config = null, $reserved = null)
    {
        if ($config) {
            parent::__construct($config, $reserved);
        }
        $this->config = Configuration::loadFromArray($config);
        $this->userIdAttr = $this->config->getString(self::UID_ATTR, null);
        $this->voShortName = $this->config->getString(self::VO_SHORT_NAME, null);
        $this->idpEntityId = $this->config->getString(self::IDP_ENTITY_ID, null);

        $this->adapter = new AdapterRpc();

        if (empty($this->userIdAttr)) {
            throw new Exception(
                self::DEBUG_PREFIX .
                'Invalid configuration: no attribute configured for extracting UID. Use option \'' . self::UID_ATTR .
                '\' to configure list of attributes, that should be considered as IDs for a user'
            );
        }

        if (empty($this->voShortName)) {
            throw new Exception(
                self::DEBUG_PREFIX .
                'Invalid configuration: no vo_short_name configured. Use option \'' . self::VO_SHORT_NAME .
                '\' to configure VO in which member should be created.'
            );
        }

        if (empty($this->idpEntityId)) {
            throw new Exception(
                self::DEBUG_PREFIX .
                'Invalid configuration: no idp_entity_id configured. Use option \'' . self::IDP_ENTITY_ID .
                '\' to configure idp for which this filter will be performed.'
            );
        }
    }

    public function process(&$request)
    {
        if (!empty($request['perun']['user'])) {
            Logger::debug(
                self::DEBUG_PREFIX .
                'User ' . $request['perun']['user']->getId() . ' already exists in Perun, skipping creation.'
            );
            return;
        }

        if (isset($request[PerunConstants::ATTRIBUTES][$this->userIdAttr][0])) {
            $uid = $request[PerunConstants::ATTRIBUTES][$this->userIdAttr][0];
        } else {
            throw new Exception(
                self::DEBUG_PREFIX . 'missing user id attribute in request.'
            );
        }

        $alreadyMember = false;
        $wasException = false;
        try {
            $extSource = $this->adapter->getExtSourceByName($this->idpEntityId);
            $vo = $this->adapter->getVoByShortName($this->voShortName);
            $this->adapter->createMember($vo->getId(), $extSource["id"], $uid);

            Logger::debug(
                self::DEBUG_PREFIX .
                'Created new member in VO ' . $vo->getId() . ' for extSource \'' . $extSource["id"]
                . '\' with login \'' . $uid . '\'.'
            );
        } catch (PerunException $e) {
            if ($e->getName() === 'AlreadyMemberException') {
                $alreadyMember = true;
            } else {
                $wasException = true;
            }
        }
        if ($alreadyMember) {
            Logger::debug(
                self::DEBUG_PREFIX .
                'User ' . $uid . ' already exists in Perun, skipping creation.'
            );
            return;
        }

        $stateId = State::saveState($request, self::STAGE);
        $url = Module::getModuleURL(self::REDIRECT_URL);

        $params = [];
        $params[self::PARAM_WAS_EXCEPTION] = $wasException;
        $params[self::PARAM_CALLBACK] = Module::getModuleURL(self::CALLBACK_URL, [
            self::PARAM_STATE_ID => $stateId,
        ]);

        HTTP::redirectTrustedURL($url, $params);
    }
}
