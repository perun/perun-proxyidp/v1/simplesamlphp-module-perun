<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Utils\HTTP;
use SimpleSAML\Module\perun\databaseCommand\BanDatabaseCommand;

class IsBanned extends ProcessingFilter
{
    private const STAGE = "perun:IsBanned";

    private const DEBUG_PREFIX = self::STAGE . ' - ';

    public const IS_BANNED = "isBanned";

    public const CONFIG_FILE_NAME = 'module_perun.php';

    public const REDIRECT_URL = 'redirect_url';

    public const TABLE_NAME = 'table_name';

    public const COL_NAME = 'column_name';

    private $redirectUrl;

    private $tableName;

    private $colName;

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $config = Configuration::loadFromArray($config);

        if ($config === null) {
            throw new Exception(
                'perun:IsBanned: Property  \'' . self::IS_BANNED . '\' is missing or invalid!'
            );
        }

        $this->redirectUrl = $config->getString(self::REDIRECT_URL, '');
        $this->tableName = $config->getString(self::TABLE_NAME);
        $this->colName = $config->getString(self::COL_NAME);
    }

    public function process(&$request)
    {
        if (isset($request['perun']['user'])) {
            $user = $request['perun']['user'];
        } else {
            throw new Exception(
                'perun:IsBanned: ' . 'missing mandatory field \'perun.user\' in request.' .
                'Hint: Did you configure PerunIdentity filter before this filter?'
            );
        }

        $userId =  $user->getId();
        if ($this->banExists($userId)) {
            Logger::info('perun:IsBanned: user ' . $userId . ' is banned, redirecting to configured URL');
            HTTP::redirectTrustedURL($this->redirectUrl);
        } else {
            Logger::debug('perun:IsBanned: user ' . $userId . ' is not banned');
        }
    }

    private function banExists($userId)
    {
        Logger::debug(self::DEBUG_PREFIX . "\n IsBanned\n");
        $dbCmd = new BanDatabaseCommand();

        $banId = $dbCmd->getBanForID($this->tableName, $userId, $this->colName);
        return $banId !== false;
    }
}
