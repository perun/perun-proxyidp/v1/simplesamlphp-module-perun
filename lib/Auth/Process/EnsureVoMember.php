<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Logger;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Adapter;
use SimpleSAML\Module\perun\CommonUtils;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\Utils\HTTP;

class EnsureVoMember extends ProcessingFilter
{
    public const STAGE = 'perun:EnsureVoMember';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const CALLBACK = 'perun/ensure_vo_member_callback.php';

    public const REDIRECT = 'perun/ensure_vo_member.php';

    //REUSED
    public const TEMPLATE = 'perun:perun-ensure-member-tpl.php';

    public const ENSURE_VO_MEMBER = 'ensureVoMember';

    public const PARAM_STATE_ID = PerunConstants::STATE_ID;

    public const PARAM_REGISTRATION_URL = 'registrationUrl';

    public const PARAM_REGISTRATION_VO = 'registrationVo';

    public const PARAM_PARAMS = 'params';

    public const PARAM_SERVICE_LOGIN_URL = 'serviceLoginUrl';

    public const TRIGGER_ATTR = 'triggerAttr';

    public const VO_DEFS_ATTR = 'voDefsAttr';

    public const REGISTRAR_URL = 'registrarURL';

    public const INTERFACE_PROPNAME = 'interface';

    public const RPC = 'rpc';

    public const LOGIN_URL_ATTR = 'loginUrlAttr';

    private $config;

    private $triggerAttr;

    private $voDefsAttr;

    private $adapter;

    private $rpcAdapter;

    private $registrarUrl;

    private $loginUrlAttr;

    public function __construct($config, $reserved)
    {
        parent::__construct($config, $reserved);
        $this->config = $config;
        $config = Configuration::loadFromArray($config);

        if ($config === null) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Property  \'' . self::ENSURE_VO_MEMBER . '\' is missing or invalid!'
            );
        }

        $this->triggerAttr = $config->getString(self::TRIGGER_ATTR, '');

        if (empty($this->triggerAttr)) {
            throw new Exception(self::DEBUG_PREFIX . 'Missing configuration option \'' . self::TRIGGER_ATTR . '\'');
        }

        $this->voDefsAttr = $config->getString(self::VO_DEFS_ATTR, '');

        if (empty($this->voDefsAttr)) {
            throw new Exception(self::DEBUG_PREFIX . 'Missing configuration option \'' . self::VO_DEFS_ATTR . '\'');
        }

        $this->loginUrlAttr = $config->getString(self::LOGIN_URL_ATTR, '');

        $this->registrarUrl = $config->getString(self::REGISTRAR_URL, null);

        $interface = $config->getString(self::INTERFACE_PROPNAME, self::RPC);
        $this->adapter = Adapter::getInstance($interface);
        $this->rpcAdapter = Adapter::getInstance(Adapter::RPC);
    }

    public function process(&$request)
    {
        if (!isset($request['SPMetadata']['entityid'])) {
            throw new Exception(
                self::DEBUG_PREFIX . 'Cannot find entityID of remote SP. ' .
                'hint: Do you have this filter in IdP context?'
            );
        }

        if (isset($request['perun']['user'])) {
            $user = $request['perun']['user'];
        } else {
            throw new Exception(
                self::DEBUG_PREFIX . 'missing mandatory field \'perun.user\' in request.' .
                'Hint: Did you configured PerunIdentity filter before this filter?'
            );
        }

        $facility = CommonUtils::getFacilityFromRequest($request, $this->adapter);

        if ($facility === null) {
            Logger::debug(self::DEBUG_PREFIX . 'skip execution - no facility provided');

            return;
        }

        $facilityAttributes = [$this->voDefsAttr, $this->triggerAttr];
        if (!empty($this->loginUrlAttr)) {
            $facilityAttributes[] = $this->loginUrlAttr;
        }

        $attrValues = $this->adapter->getFacilityAttributesValues(
            $facility,
            $facilityAttributes
        );

        $triggerAttrValue = $attrValues[$this->triggerAttr];
        if ($triggerAttrValue === null || $triggerAttrValue === false) {
            Logger::debug(
                self::DEBUG_PREFIX . 'skip execution - attribute ' . self::TRIGGER_ATTR . ' is null or false'
            );

            return;
        }

        $voShortName = $attrValues[$this->voDefsAttr];
        if (empty($voShortName)) {
            Logger::debug(
                self::DEBUG_PREFIX . 'skip execution - attribute ' . self::VO_DEFS_ATTR . ' has null or no value'
            );
            return;
        }

        $canAccess = $this->adapter->isUserInVo($user, $voShortName);
        if ($canAccess) {
            Logger::debug(self::DEBUG_PREFIX . 'user allowed to continue');
        } else {
            $this->redirect($request, $voShortName, $attrValues);
        }
    }

    private function redirect(&$request, $voShortName, $facilityAttrValues)
    {
        if (
            !empty($voShortName) &&
            !empty($this->registrarUrl) &&
            $this->rpcAdapter->hasRegistrationFormByVoShortName($voShortName)
        ) {
            $this->redirectToRegistration($request, $voShortName, $facilityAttrValues);
        } else {
            Logger::debug(
                self::DEBUG_PREFIX .
                'User is not valid in vo/group and cannot be sent to the registration - sending to unauthorized'
            );
            PerunIdentity::unauthorized($request);
        }
    }

    private function redirectToRegistration(&$request, $voShortName, $facilityAttrValues)
    {
        $request[PerunConstants::CONTINUE_FILTER_CONFIG] = $this->config;
        $request[self::STAGE] = [
            self::PARAM_REGISTRATION_URL => $this->registrarUrl,
            self::PARAM_REGISTRATION_VO => $voShortName,
        ];

        if (!empty($this->loginUrlAttr)) {
            $serviceLoginUrl = empty(
                $facilityAttrValues[$this->loginUrlAttr]
            ) ? '' : $facilityAttrValues[$this->loginUrlAttr];
            $request[self::STAGE][self::PARAM_SERVICE_LOGIN_URL] = $serviceLoginUrl;
        }

        $stateId = State::saveState($request, self::STAGE);

        $redirectUrl = Module::getModuleURL(self::REDIRECT, [self::PARAM_STATE_ID => $stateId]);
        Logger::debug(self::DEBUG_PREFIX . 'Redirecting to \'' . $redirectUrl . '\'');
        HTTP::redirectTrustedURL($redirectUrl);
    }
}
