<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun\Auth\Process;

use Firebase\JWT\JWT;
use SimpleSAML\Auth\ProcessingFilter;
use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Error\Exception;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Adapter;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\Utils\HTTP;

class AUPManager extends ProcessingFilter
{
    public const STAGE = 'perun:AUPManager';

    public const DEBUG_PREFIX = self::STAGE . ' - ';

    public const PARAM_STATE_ID = PerunConstants::STATE_ID;

    public const INTERFACE_PROPNAME = 'interface';

    public const ACCEPT_AUPS_MESSAGE = 'accept_aups_message';

    public const PARAM_CALLBACK_URL = 'callback_url';

    public const PARAM_NONCE = 'nonce';

    public const PARAM_USER_ID = 'user_id';

    public const PARAM_ENTITY_TYPE_ID = 'entity_type_id';


    public const REDIRECT_URL = 'redirect_url';

    public const RESULT_CHECK_URL = 'result_check_url';

    public const SIGNING_KEY = 'signing_key';

    public const SIGNING_ALG = 'signing_alg';

    public const CALLBACK = 'perun/aup_manager_callback.php';

    public const NOT_ACCEPTED_TPL = 'perun/aup-manager-403-tpl-php';

    private $adapter;

    private $redirectUrl;

    private $resultCheckUrl;

    private $signingKey;

    private $signingAlg;

    private $acceptAupsMessage;


    public function __construct(&$config, $reserved)
    {
        parent::__construct($config, $reserved);
        $config = Configuration::loadFromArray($config);

        $interface = $config->getString(self::INTERFACE_PROPNAME, Adapter::RPC);
        $this->adapter = Adapter::getInstance($interface);

        $this->redirectUrl = $config->getString(self::REDIRECT_URL, null);

        if (empty($this->redirectUrl)) {
            throw new Exception(sprintf("%sMissing '%s' configuration.", self::DEBUG_PREFIX, self::REDIRECT_URL));
        }

        $this->resultCheckUrl = $config->getString(self::RESULT_CHECK_URL, null);

        if (empty($this->resultCheckUrl)) {
            throw new Exception(sprintf("%sMissing '%s' configuration.", self::DEBUG_PREFIX, self::RESULT_CHECK_URL));
        }

        $this->signingKey = $config->getString(self::SIGNING_KEY, null);

        if (empty($this->signingKey)) {
            throw new Exception(sprintf("%sMissing '%s' configuration.", self::DEBUG_PREFIX, self::SIGNING_KEY));
        }

        $this->signingAlg = $config->getString(self::SIGNING_ALG, "RS256");
        $this->acceptAupsMessage = $config->getString(self::ACCEPT_AUPS_MESSAGE, null);
    }

    public function process(&$request)
    {
        $userId = $request[PerunConstants::PERUN][PerunConstants::USER]->getId();

        if (
            empty($request[PerunConstants::SP_METADATA]) ||
            empty($request[PerunConstants::SP_METADATA][PerunConstants::SP_METADATA_ENTITYID])
        ) {
            throw new Exception(self::DEBUG_PREFIX . "Missing SP metadata in state.");
        }

        $facility = $this->adapter->getFacilityByEntityId(
            $request[PerunConstants::SP_METADATA][PerunConstants::SP_METADATA_ENTITYID]
        );
        if ($facility == null) {
            throw new Exception(self::DEBUG_PREFIX . "Facility not found.");
        }

        $actualTime = strval(time());
        $randomString = $actualTime . bin2hex(random_bytes(4));

        $request[self::STAGE][self::PARAM_USER_ID] = $userId;
        $request[self::STAGE][self::SIGNING_KEY] = $this->signingKey;
        $request[self::STAGE][self::RESULT_CHECK_URL] = $this->resultCheckUrl;
        $request[self::STAGE][self::SIGNING_ALG] = $this->signingAlg;

        $stateId = State::saveState($request, self::STAGE, true);

        $callback = Module::getModuleURL(self::CALLBACK, [
            self::PARAM_STATE_ID => $stateId,
        ]);

        $payload = [
            self::PARAM_NONCE => $stateId . ';' . $randomString,
            self::PARAM_USER_ID => $userId,
            self::PARAM_ENTITY_TYPE_ID => 'facility:' . $facility->getId(),
            self::PARAM_CALLBACK_URL => $callback,
        ];

        if (!empty($this->acceptAupsMessage)) {
            $payload[self::ACCEPT_AUPS_MESSAGE] = $this->acceptAupsMessage;
        }

        $jwt = JWT::encode($payload, $this->signingKey, $this->signingAlg);

        $url = $this->redirectUrl . '/' . $jwt;
        HTTP::redirectUntrustedURL($url);
    }
}
