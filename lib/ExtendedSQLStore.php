<?php

declare(strict_types=1);

namespace SimpleSAML\Module\perun;

use PDO;
use PDOException;
use SimpleSAML\Configuration;
use SimpleSAML\Logger;
use SimpleSAML\Session;
use SimpleSAML\Store\SQL;

/**
 * Session store for SimpleSAMLphp,
 * with added compression of data,
 * and extra attributes on stored sessions.
 * For this to work, you need to set
 * 'store.idps' in config/config.php
 * and PostgreSQL has to be used.
 */
class ExtendedSQLStore extends SQL
{
    private const COMPRESSED_PREFIX = '!!GZIP_COMPRESSED!!';

    /**
     * Associative array of table versions.
     *
     * @var array
     */
    private $tableVersions;

    private $idps;

    /**
     * Initialize the table-version table.
     * @return void
     */
    private function initTableVersionTable(): void
    {
        $this->tableVersions = [];

        try {
            $fetchTableVersion = $this->pdo->query('SELECT _name, _version FROM ' . $this->prefix . '_tableVersion');
        } catch (PDOException $e) {
            $this->pdo->exec(
                'CREATE TABLE ' . $this->prefix .
                '_tableVersion (_name VARCHAR(30) NOT NULL UNIQUE, _version INTEGER NOT NULL)'
            );
            return;
        }

        while (($row = $fetchTableVersion->fetch(PDO::FETCH_ASSOC)) !== false) {
            $this->tableVersions[$row['_name']] = (int) $row['_version'];
        }
    }

    /**
     * Initialize key-value table.
     * @return void
     */
    private function initKVTable(): void
    {
        $current_version = $this->getTableVersion('kvstore');

        $text_t = 'TEXT';
        $time_field = 'TIMESTAMP';
        if ($this->driver === 'mysql') {
            // TEXT data type has size constraints that can be hit at some point, so we use LONGTEXT instead
            $text_t = 'LONGTEXT';
        }
        if ($this->driver === 'sqlsrv') {
            // TIMESTAMP will not work for MSSQL. TIMESTAMP is automatically generated and cannot be inserted
            //    so we use DATETIME instead
            $time_field = 'DATETIME';
        }

        /**
         * Queries for updates, grouped by version.
         * New updates can be added as a new array in this array
         */
        $table_updates = [
            [
                'CREATE TABLE ' . $this->prefix .
                '_kvstore (_type VARCHAR(30) NOT NULL, _key VARCHAR(50) NOT NULL, _value ' . $text_t .
                ' NOT NULL, _expire ' . $time_field . ', PRIMARY KEY (_key, _type))',
                $this->driver === 'sqlite' || $this->driver === 'sqlsrv' || $this->driver === 'pgsql' ?
                'CREATE INDEX ' . $this->prefix . '_kvstore_expire ON ' . $this->prefix . '_kvstore (_expire)' :
                'ALTER TABLE ' . $this->prefix . '_kvstore ADD INDEX ' . $this->prefix . '_kvstore_expire (_expire)'
            ],
            /**
             * This upgrade removes the default NOT NULL constraint on the _expire field in MySQL.
             * Because SQLite does not support field alterations, the approach is to:
             *     Create a new table without the NOT NULL constraint
             *     Copy the current data to the new table
             *     Drop the old table
             *     Rename the new table correctly
             *     Read the index
             */
            [
                'CREATE TABLE ' . $this->prefix .
                '_kvstore_new (_type VARCHAR(30) NOT NULL, _key VARCHAR(50) NOT NULL, _value ' . $text_t .
                ' NOT NULL, _expire ' . $time_field . ' NULL, PRIMARY KEY (_key, _type))',
                'INSERT INTO ' . $this->prefix . '_kvstore_new SELECT * FROM ' . $this->prefix . '_kvstore',
                'DROP TABLE ' . $this->prefix . '_kvstore',
                // FOR MSSQL use EXEC sp_rename to rename a table (RENAME won't work)
                $this->driver === 'sqlsrv' ?
                'EXEC sp_rename ' . $this->prefix . '_kvstore_new, ' . $this->prefix . '_kvstore' :
                'ALTER TABLE ' . $this->prefix . '_kvstore_new RENAME TO ' . $this->prefix . '_kvstore',
                $this->driver === 'sqlite' || $this->driver === 'sqlsrv' || $this->driver === 'pgsql' ?
                'CREATE INDEX ' . $this->prefix . '_kvstore_expire ON ' . $this->prefix . '_kvstore (_expire)' :
                'ALTER TABLE ' . $this->prefix . '_kvstore ADD INDEX ' . $this->prefix . '_kvstore_expire (_expire)'
            ]
        ];

        $latest_version = count($table_updates);

        if ($current_version == $latest_version) {
            return;
        }

        // Only run queries for after the current version
        $updates_to_run = array_slice($table_updates, $current_version);

        foreach ($updates_to_run as $version_updates) {
            foreach ($version_updates as $query) {
                $this->pdo->exec($query);
            }
        }

        $this->setTableVersion('kvstore', $latest_version);
    }

    /**
     * Get table version.
     *
     * @param string $name Table name.
     *
     * @return int The table version, or 0 if the table doesn't exist.
     */
    public function getTableVersion($name)
    {
        assert(is_string($name));

        if (!isset($this->tableVersions[$name])) {
            return 0;
        }

        return $this->tableVersions[$name];
    }


    /**
     * Set table version.
     *
     * @param string $name Table name.
     * @param int $version Table version.
     * @return void
     */
    public function setTableVersion($name, $version)
    {
        assert(is_string($name));
        assert(is_int($version));

        $this->insertOrUpdate(
            $this->prefix . '_tableVersion',
            ['_name'],
            ['_name' => $name, '_version' => $version]
        );
        $this->tableVersions[$name] = $version;
    }

    /** @override */
    public function __construct()
    {
        $config = Configuration::getInstance();

        $dsn = $config->getString('store.sql.dsn');
        $username = $config->getString('store.sql.username', null);
        $password = $config->getString('store.sql.password', null);
        $options = $config->getArray('store.sql.options', null);
        $this->prefix = $config->getString('store.sql.prefix', 'simpleSAMLphp');
        $this->idps = $config->getArray('store.idps', null);
        try {
            $this->pdo = new PDO($dsn, $username, $password, $options);
        } catch (PDOException $e) {
            throw new \Exception("Database error: " . $e->getMessage());
        }
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->driver = $this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME);
        if ($this->driver !== 'pgsql') {
            throw new \Exception('PostgreSQL database has to be used with perun ExtendedSQLStore');
        }

        $this->initTableVersionTable();
    }

    private function getValueFromDB($query, $params)
    {
        $query = $this->pdo->prepare($query);
        $query->execute($params);

        $row = $query->fetch(PDO::FETCH_ASSOC);
        if ($row === false) {
            return null;
        }

        $value = $row['_value'];
        if (is_resource($value)) {
            $value = stream_get_contents($value);
        }
        if (substr($value, 0, strlen(self::COMPRESSED_PREFIX)) === self::COMPRESSED_PREFIX) {
            $value = substr($value, strlen(self::COMPRESSED_PREFIX));
            $value = @gzdecode(@base64_decode($value));
        } else {
            $value = urldecode($value);
        }
        if ($value === false) {
            return null;
        }
        $value = unserialize($value);
        if ($value === false) {
            return null;
        }
        return $value;
    }

    /** @override */
    public function get($type, $key)
    {
        assert(is_string($type));
        assert(is_string($key));

        if (strlen($key) > 50) {
            $key = sha1($key);
        }

        $query = 'SELECT _value FROM ' . $this->prefix .
            '_kvstore WHERE _type = :type AND _key = :key AND (_expire IS NULL OR _expire > :now)';
        $params = ['type' => $type, 'key' => $key, 'now' => gmdate('Y-m-d H:i:s')];
        $value = $this->getValueFromDB($query, $params);

        return $value;
    }

    /**
     * Clean the key-value table of expired entries.
     * @return void
     */
    private function cleanSessionStore(): void
    {
        Logger::debug('store.sql: Cleaning key-value store.');

        $query = 'DELETE FROM ' . $this->prefix . '_kvstore WHERE _expire < :now';
        $params = ['now' => gmdate('Y-m-d H:i:s')];

        $query = $this->pdo->prepare($query);
        $query->execute($params);
    }

    /** @override */
    public function set($type, $key, $value, $expire = null)
    {
        assert(is_string($type));
        assert(is_string($key));
        assert($expire === null || (is_int($expire) && $expire > 2592000));

        if (rand(0, 1000) < 10) {
            $this->cleanSessionStore();
        }

        if (strlen($key) > 50) {
            $key = sha1($key);
        }

        if ($expire !== null) {
            $expire = gmdate('Y-m-d H:i:s', $expire);
        }

        $eppn = null;
        $sessionIndexes = [];
        $sessionIndexesDetail = [];
        if ($this->idps && $value instanceof Session) {
            foreach ($value->getAuthorities() as $authority) {
                $attributes = $value->getAuthData($authority, 'Attributes');
                if (!empty($attributes['eduPersonPrincipalName'][0])) {
                    $eppn = $attributes['eduPersonPrincipalName'][0];
                }
            }

            foreach (
                $this->idps as $idp
            ) {
                $sessionIndexesDetail[$idp] = [];
                foreach ($value->getAssociations("saml2:$idp") as $assoc) {
                    $spValue = $assoc['saml:entityID'];
                    if (
                        (!isset($assoc['Expires']) || $assoc['Expires'] >= time())
                        && !empty($assoc['saml:SessionIndex'])
                    ) {
                        $sessionIndexesDetail[$idp][$spValue] = ["saml:SessionIndex" => $assoc['saml:SessionIndex']];

                        if (!empty($assoc['saml:NameID'])) {
                            $sessionIndexesDetail[$idp][$spValue]["saml:NameID"] = $assoc['saml:NameID']->getValue();
                        }

                        $sessionIndexes[] = $assoc['saml:SessionIndex'];
                    }
                }
                if (empty($sessionIndexesDetail[$idp])) {
                    unset($sessionIndexesDetail[$idp]);
                }
            }
        }

        $value = self::COMPRESSED_PREFIX . base64_encode(gzencode(serialize($value)));

        $data = [
            '_type'   => $type,
            '_key'    => $key,
            '_value'  => $value,
            '_expire' => $expire,
        ];

        if ($eppn !== null) {
            $data['eduperson_principal_name'] = $eppn;
        }
        if (!empty($sessionIndexes)) {
            $data['session_indexes'] = '{' . implode(',', array_map(function ($v) {
                return str_replace("'", '', $v);
            }, $sessionIndexes)) . '}';
            $data['session_indexes_detail'] = json_encode($sessionIndexesDetail);
        }

        $this->insertOrUpdate($this->prefix . '_kvstore', ['_type', '_key'], $data);
    }

    /**
     * used in a SSP patch to perform SLO inside an iframe
     */
    public function getSessionBySessionIndex($sessionIndex)
    {
        assert(is_string($sessionIndex));

        $query = 'SELECT _value FROM ' . $this->prefix .
            '_kvstore WHERE :session_index = ANY (session_indexes)'; // AND (_expire IS NULL OR _expire > :now)
        $params = ['session_index' => $sessionIndex]; // , 'now' => gmdate('Y-m-d H:i:s')

        $value = $this->getValueFromDB($query, $params);
    }

    /**
     * Delete all sessions of the Perun user.
     *
     * @return int number of deleted sessions
     */
    public function deleteSessionsOfUser($eppn)
    {
        $data = [
            '_type' => 'session',
            'eppn' => $eppn,
        ];

        $query = 'DELETE FROM ' . $this->prefix . '_kvstore WHERE _type=:_type AND eduperson_principal_name=:eppn';
        $query = $this->pdo->prepare($query);
        $query->execute($data);
        return $query->rowCount();
    }
}
