<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Auth\Process\PerunEnsureMember;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, PerunEnsureMember::TEMPLATE);

$stateId = $_REQUEST[PerunEnsureMember::PARAM_STATE_ID];
$state = State::loadState($stateId, PerunEnsureMember::STAGE);

$callback = Module::getModuleURL(PerunEnsureMember::CALLBACK, [
    PerunEnsureMember::PARAM_STATE_ID => $stateId,
]);

$data = $state[PerunEnsureMember::STAGE];

$t->data[PerunEnsureMember::PARAM_REGISTRATION_URL] = $data[PerunEnsureMember::PARAM_REGISTRATION_URL];
$params = [
    PerunConstants::TARGET_NEW => $callback,
    PerunConstants::TARGET_EXISTING => $callback,
    PerunConstants::TARGET_EXTENDED => $callback,
    PerunConstants::VO => $data[PerunEnsureMember::PARAM_REGISTRATION_VO],
];

if (!empty($data[PerunEnsureMember::PARAM_REGISTRATION_GROUP])) {
    $params[PerunConstants::GROUP] = $data[PerunEnsureMember::PARAM_REGISTRATION_GROUP];
}

$t->data[PerunEnsureMember::PARAM_PARAMS] = $params;

$t->show();
