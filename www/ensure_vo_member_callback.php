<?php

declare(strict_types=1);

use SimpleSAML\Auth\ProcessingChain;
use SimpleSAML\Auth\State;
use SimpleSAML\Error\BadRequest;
use SimpleSAML\Module\perun\Auth\Process\EnsureVoMember;
use SimpleSAML\Module\perun\PerunConstants;

if (empty($_REQUEST[EnsureVoMember::PARAM_STATE_ID])) {
    throw new BadRequest('Missing required \'' . EnsureVoMember::PARAM_STATE_ID . '\' query parameter.');
}

$state = State::loadState($_REQUEST[EnsureVoMember::PARAM_STATE_ID], EnsureVoMember::STAGE);

$filterConfig = $state[PerunConstants::CONTINUE_FILTER_CONFIG];

$ensureVoMember = new EnsureVoMember($filterConfig, null);
$ensureVoMember->process($state);

// we have not been redirected, continue processing
ProcessingChain::resumeProcessing($state);
