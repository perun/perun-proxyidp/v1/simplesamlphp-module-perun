<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Module\perun\Auth\Process\PerunCreateMember;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, PerunCreateMember::TEMPLATE_URL);
$t->data[PerunCreateMember::PARAM_CALLBACK] = $_REQUEST[PerunCreateMember::PARAM_CALLBACK];
$t->data[PerunCreateMember::PARAM_WAS_EXCEPTION] = $_REQUEST[PerunCreateMember::PARAM_WAS_EXCEPTION];

$t->show();
