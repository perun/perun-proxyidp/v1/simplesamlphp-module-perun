<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Module\perun\AdapterRpc;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();

$t = new Template($config, 'perun:lshostel/username_reminder-tpl.php');
$t->show();
