<?php

use SimpleSAML\Auth\ProcessingChain;
use SimpleSAML\Auth\State;
use SimpleSAML\Error\BadRequest;
use SimpleSAML\Module\perun\Auth\Process\PerunCreateMember;

if (empty($_REQUEST[PerunCreateMember::PARAM_STATE_ID])) {
    throw new BadRequest('Missing required \'' . PerunCreateMember::PARAM_STATE_ID . '\' query parameter.');
}

$state = State::loadState($_REQUEST[PerunCreateMember::PARAM_STATE_ID], PerunCreateMember::STAGE);

ProcessingChain::resumeProcessing($state);
