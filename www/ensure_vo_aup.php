<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Auth\Process\EnsureVoAup;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, EnsureVoAup::TEMPLATE);

$stateId = $_REQUEST[EnsureVoAup::PARAM_STATE_ID];
$state = State::loadState($stateId, EnsureVoAup::STAGE);

$callback = Module::getModuleURL(EnsureVoAup::CALLBACK, [
    EnsureVoAup::PARAM_STATE_ID => $stateId,
]);

$data = $state[EnsureVoAup::STAGE];

$t->data[EnsureVoAup::PARAM_APPROVAL_URL] = $data[EnsureVoAup::PARAM_APPROVAL_URL];
$params = [
    PerunConstants::TARGET_NEW => $callback,
    PerunConstants::TARGET_EXISTING => $callback,
    PerunConstants::TARGET_EXTENDED => $callback,
    PerunConstants::VO => $data[EnsureVoAup::PARAM_APPROVAL_VO],
];

if (!empty($data[EnsureVoAup::PARAM_APPROVAL_GROUP])) {
    $params[PerunConstants::GROUP] = $data[EnsureVoAup::PARAM_APPROVAL_GROUP];
}

$t->data[EnsureVoAup::PARAM_PARAMS] = $params;

$t->show();
