<?php

declare(strict_types=1);

use SimpleSAML\Auth\State;
use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\Auth\Process\PerunAup;
use SimpleSAML\Module\perun\PerunConstants;
use SimpleSAML\XHTML\Template;

$config = Configuration::getInstance();
$t = new Template($config, PerunAup::TEMPLATE);

$stateId = $_REQUEST[PerunAup::PARAM_STATE_ID];
$state = State::loadState($stateId, PerunAup::STAGE);

$callback = Module::getModuleURL(PerunAup::CALLBACK, [
    PerunAup::PARAM_STATE_ID => $stateId,
]);

$data = $state[PerunAup::STAGE];

$t->data[PerunAup::PARAM_APPROVAL_URL] = $data[PerunAup::PARAM_APPROVAL_URL];
$params = [
    PerunConstants::TARGET_NEW => $callback,
    PerunConstants::TARGET_EXISTING => $callback,
    PerunConstants::TARGET_EXTENDED => $callback,
    PerunConstants::VO => $data[PerunAup::PARAM_APPROVAL_VO],
];

if (!empty($data[PerunAup::PARAM_APPROVAL_GROUP])) {
    $params[PerunConstants::GROUP] = $data[PerunAup::PARAM_APPROVAL_GROUP];
}

$t->data[PerunAup::PARAM_PARAMS] = $params;

$t->show();
