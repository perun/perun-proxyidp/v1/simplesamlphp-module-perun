-- Script for creating tables for AuthEventLogging filter
CREATE TABLE logging_idp (
    id SERIAL PRIMARY KEY,
    identifier VARCHAR(255) UNIQUE,
    name VARCHAR(255)
);

CREATE TABLE logging_sp (
    id SERIAL PRIMARY KEY,
    identifier VARCHAR(255) UNIQUE,
    name VARCHAR(255)
);

CREATE TABLE session_id_values (
    id SERIAL PRIMARY KEY,
    value VARCHAR(255) UNIQUE
);

CREATE TABLE requested_acrs_values (
    id SERIAL PRIMARY KEY,
    value VARCHAR(255) UNIQUE
);

CREATE TABLE upstream_acrs_values (
    id SERIAL PRIMARY KEY,
    value VARCHAR(255) UNIQUE
);

CREATE TABLE user_agent_raw_values (
    id SERIAL PRIMARY KEY,
    value VARCHAR(255) UNIQUE
);

CREATE TABLE user_agent_values (
    id SERIAL PRIMARY KEY,
    value VARCHAR(255) UNIQUE
);

CREATE TABLE auth_event_logging (
    id SERIAL PRIMARY KEY,
    day TIMESTAMP NOT NULL DEFAULT NOW(),
    "user" VARCHAR(255),
    user_id INT,
    idp_id INT,
    sp_id INT,
    ip_address VARCHAR(255),
    geolocation_city VARCHAR(255),
    geolocation_country VARCHAR(255),
    local_mfa_performed BOOLEAN DEFAULT FALSE,
    session_id INT,
    requested_acrs_id INT,
    upstream_acrs_id INT,
    user_agent_raw_id INT,
    user_agent_id INT,
    FOREIGN KEY (idp_id) REFERENCES logging_idp(id),
    FOREIGN KEY (sp_id) REFERENCES logging_sp(id),
    FOREIGN KEY (session_id) REFERENCES session_id_values(id),
    FOREIGN KEY (requested_acrs_id) REFERENCES requested_acrs_values(id),
    FOREIGN KEY (upstream_acrs_id) REFERENCES upstream_acrs_values(id),
    FOREIGN KEY (user_agent_id) REFERENCES user_agent_values(id),
    FOREIGN KEY (user_agent_raw_id) REFERENCES user_agent_raw_values(id)
);
