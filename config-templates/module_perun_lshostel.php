<?php

/**
 * This is example configuration for the LS AAI module.
 * Copy this file to default config directory and edit the properties.
 *
 * copy command (from SimpleSAML base dir) cp modules/lsaai/config-templates/module_lsaai.php config/.
 */

declare(strict_types=1);

$config = [
    'register_link' => '',

    'pwd_reset' => [
        'ls_username_login_entity_id' => '',
        'ls_username_login_scope' => '',
        'vo_short_name' => '',
        'perun_namespace' => '',
        'perun_url' => '',
        'perun_email_attr' => '',
    ],
];
