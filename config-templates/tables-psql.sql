-- Script for creating tables
CREATE TABLE whitelist (
    date TIMESTAMP NOT NULL DEFAULT NOW() ,
    "entityId" VARCHAR(255) NOT NULL,
    reason VARCHAR(255),
    PRIMARY KEY ("entityId")
);

CREATE TABLE greylist (
    date TIMESTAMP NOT NULL DEFAULT NOW(),
    "entityId" VARCHAR(255) NOT NULL,
    reason VARCHAR(255),
    PRIMARY KEY ("entityId")
);

CREATE TABLE scriptchallenges (
    id VARCHAR(255) NOT NULL,
    challenge VARCHAR(255) NOT NULL,
    script VARCHAR(255) NOT NULL,
    date TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE INDEX idx_date on scriptchallenges (date);
