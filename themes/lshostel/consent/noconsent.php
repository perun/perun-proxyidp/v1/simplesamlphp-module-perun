<?php

declare(strict_types=1);

use SimpleSAML\Module;

if (array_key_exists('name', $this->data['dstMetadata'])) {
    $dstName = $this->data['dstMetadata']['name'];
} elseif (array_key_exists('OrganizationDisplayName', $this->data['dstMetadata'])) {
    $dstName = $this->data['dstMetadata']['OrganizationDisplayName'];
} else {
    $dstName = $this->data['dstMetadata']['entityid'];
}
if (is_array($dstName)) {
    $dstName = $this->t($dstName);
}
$dstName = htmlspecialchars($dstName);

$this->data['header'] = $this->t('{consent:consent:noconsent_title}');

$this->data['head'] = '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/res/bootstrap/css/bootstrap.css') . '" />';
$this->data['head'] .= '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/lshostel/res/css/consent.css') . '" />';

$this->includeAtTemplateBase('includes/header.php');
?>

<p><?php echo $this->t('{consent:consent:noconsent_text}', ['SPNAME' => $dstName,]); ?> </p>

<?php if ($this->data['resumeFrom']) { ?>
    <p>
        <a class="btn btn-default" href="<?php echo htmlspecialchars($this->data['resumeFrom']); ?>">
            <i class="glyphicon glyphicon-chevron-left"></i>
            <?php echo $this->t('{consent:consent:noconsent_return}'); ?>
        </a>
    </p>
<?php } ?>

<?php
if ($this->data['aboutService']) {
    ?>
    <p>
        <a href="<?php echo htmlspecialchars($this->data['aboutService']); ?>">
            <i class="glyphicon glyphicon-info-sign"></i>
            <?php echo $this->t('consent:consent:noconsent_goto_about'); ?>
        </a>
    </p>
<?php } ?>

<p>
    <a class="btn btn-default" href="<?php echo htmlspecialchars($this->data['logoutLink']); ?>">
        <i class="glyphicon glyphicon-ban-circle"></i>
        <?php echo $this->t('{consent:consent:abort}', ['SPNAME' => $dstName,]); ?>
    </a>
</p>

<?php

$this->includeAtTemplateBase('includes/footer.php');
