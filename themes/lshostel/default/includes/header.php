<?php

/**
 * Support the htmlinject hook, which allows modules to change header, pre and post body on all pages.
 */

declare(strict_types=1);

use SimpleSAML\Module;
use SimpleSAML\Module\perun\LshostelTemplateHelper;

$this->data['htmlinject'] = [
    'htmlContentPre' => [],
    'htmlContentPost' => [],
    'htmlContentHead' => [],
];

$jquery = [];
if (array_key_exists('jquery', $this->data)) {
    $jquery = $this->data['jquery'];
}

if (array_key_exists('pageid', $this->data)) {
    $hookinfo = [
        'pre' => &$this->data['htmlinject']['htmlContentPre'],
        'post' => &$this->data['htmlinject']['htmlContentPost'],
        'head' => &$this->data['htmlinject']['htmlContentHead'],
        'jquery' => &$jquery,
        'page' => $this->data['pageid'],
    ];

    Module::callHooks('htmlinject', $hookinfo);
}
// - o - o - o - o - o - o - o - o - o - o - o - o -

/*
 * Do not allow to frame SimpleSAMLphp pages from another location.
 * This prevents clickjacking attacks in modern browsers.
 *
 * If you don't want any framing at all you can even change this to
 * 'DENY', or comment it out if you actually want to allow foreign
 * sites to put SimpleSAMLphp in a frame. The latter is however
 * probably not a good security practice.
 */
header('X-Frame-Options: SAMEORIGIN');

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0"/>
    <script type="text/javascript" src="/<?php echo $this->data['baseurlpath']; ?>resources/script.js"></script>
    <title><?php echo array_key_exists('header', $this->data) ? $this->data['header'] : 'SimpleSAMLphp'; ?></title>

    <link rel="stylesheet" type="text/css" href="/<?php echo $this->data['baseurlpath']; ?>resources/default.css"/>
    <link rel="icon" type="image/icon" href="/<?php echo $this->data['baseurlpath']; ?>resources/icons/favicon.ico"/>
    <?php

    echo LsHostelTemplateHelper::getJquery($jquery, $this->data['baseurlpath']);
    echo LsHostelTemplateHelper::getClipboard($this->data, $this->data['baseurlpath']);
    echo LsHostelTemplateHelper::getRtl($this->isLanguageRTL(), $this->data['baseurlpath']);

    if (!empty($this->data['htmlinject']['htmlContentHead'])) {
        foreach ($this->data['htmlinject']['htmlContentHead'] as $c) {
            echo $c;
        }
    }
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo Module::getModuleUrl('perun/res/bootstrap/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Module::getModuleUrl('perun/res/css/lsaai.css'); ?>"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Module::getModuleUrl('perun/lshostel/res/css/lshostel.css'); ?>"/>
    <meta name="robots" content="noindex, nofollow"/>

    <?php
    if (array_key_exists('head', $this->data)) {
        echo '<!-- head -->' . $this->data['head'] . '<!-- /head -->';
    }
    ?>
</head>
<?php
$onLoad = '';
if (array_key_exists('autofocus', $this->data)) {
    $onLoad .= 'SimpleSAML_focus(\'' . $this->data['autofocus'] . '\');';
}
if (isset($this->data['onLoad'])) {
    $onLoad .= $this->data['onLoad'];
}

if ('' !== $onLoad) {
    $onLoad = ' onload="' . $onLoad . '"';
}
?>
<body <?php echo $onLoad; ?>>

<div id="wrap" class="container">

    <div id="header">
        <img src="<?php echo Module::getModuleUrl('perun/res/img/lsaai_logo_200.png'); ?>"
             alt="Logo of the European Life Science Research Infrastructures">
        <h1><?php echo $this->data['header'] ?? 'LS Username Login'; ?></h1>
    </div>

    <div id="content">

<?php

if (!empty($this->data['htmlinject']['htmlContentPre'])) {
    foreach ($this->data['htmlinject']['htmlContentPre'] as $c) {
        echo $c;
    }
}
