<?php

declare(strict_types=1);

use SimpleSAML\Configuration;
use SimpleSAML\Module;
use SimpleSAML\Module\perun\LshostelTemplateHelper;

const REGISTER_LINK = 'register_link';

$backlink = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$conf = Configuration::getConfig(LshostelTemplateHelper::CONFIG_FILE_NAME);
$register_link = $conf->getString(REGISTER_LINK);
parse_str(parse_url($register_link, PHP_URL_QUERY), $query_params);
$query_params['targetnew'] = $query_params['targetexisting'] = $query_params['targetextended'] = $backlink;
$register_link = strtok($register_link, '?') . '?' . http_build_query($query_params);

$this->data['autofocus'] = 'username';

$this->data['head'] .=
    '<script src="' . Module::getModuleUrl('perun/res/js/jquery.js') . '" ></script>';
$this->data['head'] .=
    '<script src="' . Module::getModuleUrl('perun/res/bootstrap/js/bootstrap.min.js') . '" ></script>';
$this->data['head'] .= '<link rel="stylesheet" media="screen" type="text/css" href="' .
    Module::getModuleUrl('perun/lshostel/res/css/lshostel.css') . '" />';

$this->data['header'] = $this->t('{perun:lshostel:login_user_pass_header}');

$this->includeAtTemplateBase('includes/header.php');

?>

<div class="row pl-0 pr-0">
    <?php if (null !== $this->data['errorcode']) { ?>
        <div class="alert alert-danger">
            <p>
                    <span class="glyphicon glyphicon-exclamation-sign"
                          style="float:left; font-size: 38px; margin-right: 10px;"></span>

                <?php if ('WRONGUSERPASS' === $this->data['errorcode']) {?>
                <strong>
                    <?php
                    echo htmlspecialchars(
                        $this->t('{perun:lshostel:login_title_WRONGUSERPASS}', $this->data['errorparams'])
                    );
                    ?>
                </strong>
            </p>
            <p>
                    <?php
                    echo htmlspecialchars(
                        $this->t('{perun:lshostel:login_descr_WRONGUSERPASS}', $this->data['errorparams'])
                    );
                    ?>
            </p>
                <?php } else { ?>
                <strong>
                    <?php
                    echo htmlspecialchars(
                        $this->t(
                            '{errors:title_' . $this->data['errorcode'] . '}',
                            $this->data['errorparams']
                        )
                    );
                    ?>
                </strong>
                </p>
                <p>
                    <?php
                    echo htmlspecialchars(
                        $this->t(
                            '{errors:descr_' . $this->data['errorcode'] . '}',
                            $this->data['errorparams']
                        )
                    );
                    ?>
                </p>
                <?php } ?>
            <a class="btn btn-link" href="<?php
            echo Module::getModuleURL('perun/lshostel/pwd_reset.php'); ?>">
                <?php echo $this->t('{perun:lshostel:login_forgot_password}'); ?>
            </a>
            <br/>
            <a class="btn btn-link" href="<?php
            echo Module::getModuleURL('perun/lshostel/username_reminder.php'); ?>">
                <?php echo $this->t('{perun:lshostel:login_forgot_username}'); ?>
            </a>
        </div>
    <?php } ?>
    <div class="col-xs-12">
        <form action="?" method="post" name="f" class="form-horizontal">
            <div class="form-group">
                <label class="sr-only" for="inlineFormInputGroup">
                    <?php echo $this->t('{perun:lshostel:login_username}'); ?>
                </label>
                <div class="input-group mb-2">
                    <span class="input-group-addon">
                            <span class=" glyphicon glyphicon-user" id="basic-addon1"></span>
                    </span>
                    <input id="username" name="username" class="form-control"
                           aria-describedby="basic-addon1" placeholder="Username"
                           value="<?php echo htmlspecialchars($this->data['username']); ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="sr-only" for="inlineFormInputGroup"><?php
                    echo $this->t('{login:password}'); ?></label>
                <div class="input-group mb-2">
                    <span class="input-group-addon">
                        <span class=" glyphicon glyphicon-lock" id="basic-addon2"></span>
                    </span>
                    <input id="password" type="password" name="password" class="form-control"
                           placeholder="Password" aria-describedby="basic-addon2"/>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-success btn-block" type="submit">
                    <?php echo $this->t('{login:login_button}'); ?>
                </button>
            </div>
            <div class="form-group text-center">
                <a class="btn btn-link" href="<?php echo $register_link; ?>">
                    <?php echo $this->t('{perun:lshostel:login_login_button_register}'); ?>
                </a> |
                <a class="btn btn-link"
                   href="<?php echo Module::getModuleURL("perun/lshostel/pwd_reset.php");?>">
                    <?php echo $this->t('{perun:lshostel:login_login_button_forgotten_password}')?>
                </a> |
                <a class="btn btn-link"
                   href="<?php echo Module::getModuleURL("perun/lshostel/username_reminder.php");?>">
                  <?php echo $this->t('{perun:lshostel:login_login_button_forgotten_username}')?>
                </a>
            </div>
            <?php
            foreach ($this->data['stateparams'] as $name => $value) {
                echo '<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars(
                    $value
                ) . '" />';
            }
            ?>
        </form>
    </div>
<?php

$this->includeAtTemplateBase('includes/footer.php');
