<?php

use SimpleSAML\Module;

if (!empty($this->data['htmlinject']['htmlContentPost'])) {
    foreach ($this->data['htmlinject']['htmlContentPost'] as $c) {
        echo $c;
    }
}

?>

</div><!-- #content -->
</div><!-- #wrap -->

<div style="text-align: center;">
  <div id="footer" style="display: flex; justify-content: space-between; margin: 0 auto; max-width: 1000px;">

    <div>
      <img src="<?php echo Module::getModuleUrl('perun/res/img/elter_logo_120.png') ?>">
    </div>

    <div>
      <img src="<?php echo Module::getModuleUrl('perun/res/img/eu_logo_120.png') ?>">
    </div>

    <div>
      <p>eLTER receives funding from the European Union’s Horizon 2020 research and innovation programme under GA
        No 871126 (eLTER PPP) and GA No 871128 (eLTER PLUS), and the European Union’s Horizon Europe research and
        innovation programme under GA No 101131751 (eLTER EnRich).</p>
    </div>

  </div>
</div><!-- #footer -->

</body>
</html>
