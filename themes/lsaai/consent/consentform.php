<?php

use SimpleSAML\Module;
use SimpleSAML\Module\perun\Consent;

if (empty($this->data['htmlinject'])) {
    $this->data['htmlinject'] = [
        'htmlContentPre' => [],
        'htmlContentPost' => [],
        'consentAttributeTablePre' => []
    ];
}

if (!isset($this->data['htmlinject']['htmlContentPre'])) {
    $this->data['htmlinject']['htmlContentPre'] = [];
}

if (!isset($this->data['htmlinject']['htmlContentPost'])) {
    $this->data['htmlinject']['htmlContentPost'] = [];
}

if (!isset($this->data['htmlinject']['consentAttributeTablePre'])) {
    $this->data['htmlinject']['consentAttributeTablePre'] = [];
}

$parsedJurisdiction = Consent::getJurisdiction($this->data['dstMetadata']);

$this->data['htmlinject']['htmlContentPre'][] = Consent::getJurisdictionDialogue($parsedJurisdiction);
$this->data['htmlinject']['htmlContentPre'][] = Consent::getAcceptedTosDialogueLsaai($this->data['dstMetadata']);

$this->data['htmlinject']['htmlContentPost'][] = (
    "<p class='mt-1'>For withdrawing consent, contact <a href='mailto:support@aai.lifescience-ri.eu'>" .
        "support@aai.lifescience-ri.eu</a></p>"
);

$this->data['htmlinject']['consentAttributeTablePre'][] = Consent::getPrivacyPolicyDialogueLsaai(
    $this->data['sppp'],
    $this->data['dstMetadata'] ?? []
);

include Module::getModuleDir('perun') . '/themes/perun/consent/consentform.php';
